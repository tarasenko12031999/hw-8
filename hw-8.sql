CREATE DATABASE "hw-db_postgres"

CREATE TABLE IF NOT EXISTS user_info(
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(55) NOT NULL,
    last_name VARCHAR(55),
    age INTEGER
    );

CREATE TABLE IF NOT EXISTS address(
    id SERIAL PRIMARY KEY,
    street VARCHAR(50) NOT NULL,
    country VARCHAR(50) NOT NULL,
    city VARCHAR(50) NOT NULL,
    zip_code VARCHAR(5) NOT NULL
    );

CREATE TABLE IF NOT EXISTS location_info(
    id SERIAL PRIMARY KEY,
    name VARCHAR (55) NOT NULL,
    score INTEGER,
    address_id INTEGER NOT NULL,
    FOREIGN KEY(address_id) REFERENCES address(ID) ON DELETE CASCADE
    );

CREATE TABLE IF NOT EXISTS user_address(
    id SERIAL PRIMARY KEY,
    id_user INTEGER NOT NULL,
    id_address INTEGER NOT NULL,
    FOREIGN KEY(id_user) REFERENCES user_info(ID) ON DELETE CASCADE,
    FOREIGN KEY(id_address) REFERENCES address(ID) ON DELETE CASCADE
    );

INSERT INTO user_info(first_name, last_name, age)
VALUES ('stepan', 'kravchenko', 21),
       ('oleg', 'dubrov', 43),
       ('karina', 'denisova', 54),
       ('olga', 'hizhnyak', 14),
       ('lexa', 'singaenko', 55);

INSERT INTO address(street, country, city, zip_code)
VALUES ('pishevaya', 'ukraine', 'zaporizhzya', 69059),
       ('kosmichna', 'ukraine', 'zaporizhzya', 61350),
       ('gagarina', 'ukraine', 'kiev', 75015),
       ('pobedi', 'ukraine', 'zaporizhya', 62155),
       ('ivanova', 'ukraine', 'kharkiv', 62395);

INSERT INTO location_info(name, score, address_id)
VALUES ('Location 1', 34, 1),('Location 2', 51, 4),
       ('Location 3', 25, 1),('Location 4', 74, 2),
       ('Location 5', 148, 4), ('Location 6', 99, 3);

INSERT INTO user_address(id_user, id_address)
VALUES (2, 5),(3, 2),
       (4, 1),(1, 2),
       (5, 1);

select * from user_info as ui
	left join user_address as ua on ui.id = ua.id_user
	left join address on ua.id_address = address.id

select ui.first_name, ui.last_name, ui.age, ad.street from user_info as ui
	left join user_address as ua on ui.id = ua.id_user
	left join address as ad on ua.id_address = ad.id
	where ui.age = 55